import subprocess

try:
    with open("tests/test_input.txt", "r") as test_file, open("tests/expected_output.txt", "r") as expected_values_file: 
        # input=b"first word"
        test_number = 0
        
        for test_file_line,  expected_values_file_line in zip(test_file, expected_values_file):
            test_file_line = test_file_line.replace("\n", "")
            expected_values_file_line = expected_values_file_line.replace("\n", "")

            input_data = bytes(test_file_line, 'utf-8')
            print("------------------------")
            print("TEST_NUMBER: ", test_number)
            test_number += 1
            try:
                process = subprocess.run(['./main'], input = input_data, shell = True, check = True, timeout = 3, capture_output=True)
                if (process.stdout == bytes(expected_values_file_line, "utf-8")):
                   print("\tOK!")
                else:
                    print("Expected value: ",'"' +  expected_values_file_line + '"')     
                    print("Recieved value: ",'"' + process.stdout.decode() + '"')     
            except subprocess.CalledProcessError as e:
                # print(f"Ошибка команды {e.cmd}!")
                print("\tERROR")
                print("\tstderr:", e.stderr.decode())
                
except subprocess.CalledProcessError as e:
    print(f"Ошибка команды {e.cmd}!")
    print("stderr:", e.stderr)
    



# print(34534543)