%include "lib/lib.inc"
%include "dictionary/colon.inc"
section .text

global find_word

; Принимает два параметра: 
; (rdi) Указатель на нуль-терминированную строку.
; (rsi) Указатель на начало словаря.
;find_word пройдёт по всему словарю в поисках подходящего ключа. 
;Если подходящее вхождение найдено, вернёт адрес начала вхождения в  словарь (не значения),
; иначе вернёт 0.
find_word:
    push r12
    push r13

    mov r12, rdi ; str_ptr
    mov r13, rsi ; dict_current_ptr
    
    .loop:
    
        test r13, r13 ; проверяем, является ли данный элемент словаря последним
        je .word_not_found

        mov rsi, [colon_key_ptr(r13)]
        mov rdi, r12
        call string_equals
        test rax, rax
        jne .word_found



        mov r13, [r13]
        jmp .loop

    .word_found:
        mov rax, r13
        jmp .exit

    .word_not_found:
        xor rax, rax

    .exit:
        pop r13
        pop r12
        ret



