%define last_ptr 0

%macro colon 2

    %ifid %2
        %ifstr %1
            %2:
                dq last_ptr
                dq %%2_key ; указатель на ключ для текущего элемента словаря
                dq %%2_marker ; указатель на значение текущего элемента словаря
                
            %define last_ptr %2

            %%2_key: db %1, 0 ; нуль-терминированная строка, являющаяся ключом 
            %%2_marker: ; нуль терминированная строка, являющаяся значением
        %else
            %error "The first parameter must be a string"
        %endif
    %else
        %error "The second parameter must be a label>"
    %endif

    
%endmacro


%define colon_key_ptr(colon_ptr) colon_ptr + 8 ; указатель на ключ элемента

%define colon_marker_ptr(colon_ptr) colon_ptr + 16 ; указатель на значение элемента