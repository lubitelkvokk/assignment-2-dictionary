DICT = dictionary
LIB = lib
SRC = src
ASMFLAGS = -felf64
TESTS = tests

$(DICT)/%.o: $(DICT)/%.asm
	nasm $(ASMFLAGS) -o $@ $<

$(SRC)/%.o: $(SRC)/%.asm
	nasm $(ASMFLAGS) -o $@ $<

$(LIB)/%.o: $(LIB)/%.asm
	nasm $(ASMFLAGS) -o $@ $<


$(DICT)/dict.o: $(DICT)/colon.inc


$(SRC)/main.o: $(SRC)/main.asm $(SRC)/words.inc $(DICT)/colon.inc $(DICT)/dict.o $(DICT)/dict.inc $(LIB)/lib.inc $(LIB)/lib.o
	nasm $(ASMFLAGS) -o $@ $<


main: $(SRC)/main.o 
	ld -o $@ $(SRC)/main.o $(DICT)/dict.o $(LIB)/lib.o

# $(DICT)/dict: $(DICT)/dict.o
# 	ld -o $@ $<

test: $(TESTS)/test.py
	python3 tests/test.py





	



