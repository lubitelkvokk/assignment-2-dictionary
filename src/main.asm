
%include "dictionary/dict.inc"
%include "lib/lib.inc"

%include "dictionary/colon.inc"
%include "src/words.inc"

%define SYS_EXIT 60
%define EXIT_STATUS 1
%define MAX_STRING_LENGTH 256
%define STDOUT 1
%define STDERR 2
section .rodata
    err_line_length: db "String must be less than 256", 0
    .end:

    err_not_found: db "Such value was not found" , 0
    .end:

section .data
    key: db ""   

section .text
global _start
_start:

    mov r12, key
    xor r13, r13 ; счётчик символов (если больше 255, ошибка)
    .reading_loop:
        inc r13
        cmp r13, MAX_STRING_LENGTH ; если количество символов строки больше, переходим на метку вывода ошибки
        jg .length_error

        call read_char
        mov [r12], al
        cmp rax, 0xA
        je .next

        
        inc r12
        test rax, rax

        jne .reading_loop

    .next:
        xor rax, rax
        mov [r12], al ; если строка окончилась не 0, то заменяем, чтобы получить нуль-терминированную строку

        mov rdi, key
        mov rsi, first_word

        call find_word

        test rax, rax
        je .not_found

        mov rdi, [colon_marker_ptr(rax)] 
        mov rsi, 1
        call print_string


    .successful_exit:
        xor rdi, rdi
        call exit


    .length_error: ; выделить в отдельные директорию и файл
        mov rdi, err_line_length
        jmp .err_out

    .not_found:
        mov rdi, err_not_found
        ; jmp .err_out

    .err_out:
        mov rsi, STDERR
        call print_string

    .err_exit:
        mov rdi, EXIT_STATUS
        call exit